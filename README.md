# Maespa at the WTC3

This repository contains the code to run Maespa (actually Maestra) at the WTC3. 

## Requirements

You must have access to the HIEv. See `R/load.R` on setting the token. You also need the Maeswrap package installed. 
The Maespa executable is installed for you if you don't already have it. Only runs on Windows.